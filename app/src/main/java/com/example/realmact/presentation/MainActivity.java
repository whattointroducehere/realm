package com.example.realmact.presentation;

import android.os.Bundle;

import com.example.realmact.R;
import com.example.realmact.databinding.ActivityMainBinding;
import com.example.realmact.ui.edition.MyEditionFragment;
import com.example.realmact.ui.list.MyListFragment;

import io.realm.Realm;

public class MainActivity extends BaseActivity<ActivityMainBinding> implements IPresenter.View  {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Realm.init(this);

        if(savedInstanceState == null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.edition_container, new MyEditionFragment())
                    .add(R.id.list_container, new MyListFragment())
                    .commit();
        }
    }

    @Override
    protected int getLayoutRes() { return R.layout.activity_main; }

    @Override
    protected void initView() {

    }

    @Override
    protected void onDestroyView() {

    }

    @Override
    protected BasePresenter getPresenter() {
        return null;
    }
}
