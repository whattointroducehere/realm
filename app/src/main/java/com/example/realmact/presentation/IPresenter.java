package com.example.realmact.presentation;

import com.example.realmact.presentation.base.BasePresenter;

public interface IPresenter {
    interface View{


    }

    interface Listener extends BasePresenter<View> {


    }
}
