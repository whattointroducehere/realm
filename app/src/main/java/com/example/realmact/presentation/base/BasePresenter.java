package com.example.realmact.presentation.base;

public interface BasePresenter<V> {
    void onStartView(V view);

    void OnStopView();
}
