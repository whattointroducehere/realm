package com.example.realmact.presentation;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

public abstract class BaseActivity<Binding extends ViewDataBinding> extends AppCompatActivity {
    private Binding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, getLayoutRes());
        initView();
    }

    @LayoutRes
    protected abstract int getLayoutRes();

    protected Binding getBinding() {
        return binding;
    }

    protected abstract void initView();

    @Override
    public void onDestroy() {
        if (getPresenter() != null) {
            getPresenter().OnStopView();
            onDestroyView();
        }
        super.onDestroy();
    }

    protected abstract void onDestroyView();

    protected abstract BasePresenter getPresenter();


    protected void toast(String message){
        Toast.makeText(this,message,Toast.LENGTH_SHORT).show();
    }




}


